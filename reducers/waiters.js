import _findIndex from 'lodash/findIndex';
import update from 'immutability-helper';
import {
  WAITERS_SET_WAITERS_LIST,
  WAITERS_SET_LOADING,
  WAITERS_UPDATE_WAITER_INFO,
  WAITERS_DELETE_WAITER_BY_TG_ID,
} from 'actionTypes/waiters';

const initialState = {
  waitersList: [],
  waitersLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITERS_SET_WAITERS_LIST: {
      return {
        ...state,
        waitersList: [
          ...action.payload,
        ]
      };
    }
    case WAITERS_UPDATE_WAITER_INFO: {
      const { telegramId } = action.payload;
      const waiterIndex = _findIndex(state.waitersList, ['telegramId', telegramId]);
      return update(state, {
        waitersList: {
          [waiterIndex]: {
            $merge: action.payload
          }
        }
      });
    }
    case WAITERS_DELETE_WAITER_BY_TG_ID: {
      const index = _findIndex(state.waitersList, ['telegramId', action.payload])
      return update(state, {
        waitersList: {$splice: [[index, 1]] }
      })
    }
    case WAITERS_SET_LOADING: {
      return {
        ...state,
        waitersLoading: action.payload
      };
    }
    default:
      return state;
  }
};
