import React, {
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';
import {
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import { PageTitle, Widget } from 'components';
import { Typography } from 'components/Wrappers';

import UserTable from './components/UserTable';

import useStyles from './styles';

const Waiters = ({
  waitersLoading,
  adminsLoading,
  waitersList,
  adminsList,
  initWaitersAction,
  updateAdmin,
  updateWaiter,
  deleteWaiterByTgId,
  deleteAdminByTgId,
}) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [alertOpen, setAlertOpen] = useState(false);

  const adminHeaders = [
    'ID',
    'Telegram ID',
    t('waiters.lang'),
    t('waiters.name'),
    t('waiters.surname'),
    t('waiters.telegramName'),
    '',
  ];

  const waiterHeaders = [
    'ID',
    'Telegram ID',
    t('waiters.lang'),
    t('waiters.name'),
    t('waiters.surname'),
    t('waiters.telegramName'),
    t('waiters.onShift'),
    ''
  ];

  useEffect(() => {
    initWaitersAction()
  }, [
    initWaitersAction,
  ]);

  const adminsData = useMemo(() => (
    adminsList.map(({ _id, telegramId, first_name, last_name, username, language_code }) => ({
      id: _id,
      telegramId,
      language: language_code,
      firstName: first_name,
      lastName: last_name,
      username
    }))
  ),
    [adminsList]
  );

  const waitersData = useMemo(() => (
    waitersList.map(({ _id, telegramId, first_name, last_name, username, language_code, onDuty, qrList }) => ({
      id: _id,
      telegramId,
      language: language_code,
      firstName: first_name,
      lastName: last_name,
      username,
      onDuty,
      qrList
    }))
  ),
    [waitersList]
  );

  const onWaiterUpdate = useCallback(async (e) => {
    const response = await updateWaiter(e);
    if (response.error) {
      console.error(response);
      setAlertOpen(true);
    }
  },
    [updateWaiter]
  );

  const handleAlertClose = () => {
    setAlertOpen(false);
  };

  const handlePageReload = () => {
    window.location.reload();
  };

  return (
    <>
      <PageTitle title={t('waiters.title')} />
      <Box>
        <Widget
          upperTitle
          bodyClass={classes.fullHeightBody}
          className={classes.card}
          noHeaderPadding
        >
          <Box mt={4}>
            <Box mb={2}>
              <Typography variant='h3' color='text' noWrap>
                {t('waiters.admins')}:
              </Typography>
            </Box>
            <Box>
              {adminsLoading ? (
                <div className={classes.loaderWrapper}>
                  <CircularProgress size={42} />
                </div>
              ) : (
                <UserTable
                  userHeaders={adminHeaders}
                  userRows={adminsData}
                  updateUser={updateAdmin}
                  deleteUserByTgId={deleteAdminByTgId}
                />
              )}
            </Box>
          </Box>

          <Box mt={4}>
            <Box mb={2}>
              <Typography variant='h3' color='text' noWrap>
                {t('waiters.waiters')}:
              </Typography>
            </Box>
            <Box>
              {waitersLoading ? (
                <div className={classes.loaderWrapper}>
                  <CircularProgress size={42} />
                </div>
              ) : (
                <UserTable
                  userHeaders={waiterHeaders}
                  userRows={waitersData}
                  updateUser={onWaiterUpdate}
                  deleteUserByTgId={deleteWaiterByTgId}
                  isCollapsed
                  isWaitersTable
                />
              )}
            </Box>
          </Box>

          <Dialog
            open={alertOpen}
            onClose={handleAlertClose}
          >
            <DialogTitle>{t('waiters.error')}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t('waiters.statusChanged')}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                color='primary'
                onClick={handleAlertClose}
              >
                {t('waiters.close')}
              </Button>
              <Button
                color='primary'
                variant='contained'
                onClick={handlePageReload}
              >
                {t('waiters.refresh')}
              </Button>
            </DialogActions>
          </Dialog>
        </Widget>
      </Box>
    </>
  );
}

Waiters.defaultProps = {
  waitersList: [],
  adminsList: [],
};

Waiters.propTypes = {
  waitersLoading: PropTypes.bool.isRequired,
  adminsLoading: PropTypes.bool.isRequired,
  waitersList: PropTypes.arrayOf(
    PropTypes.shape({
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      tables: PropTypes.arrayOf(PropTypes.string),
      telegramId: PropTypes.number.isRequired,
      username: PropTypes.string,
      _id: PropTypes.string.isRequired,
    })
  ),
  adminsList: PropTypes.arrayOf(
    PropTypes.shape({
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      tables: PropTypes.arrayOf(PropTypes.string),
      telegramId: PropTypes.number.isRequired,
      username: PropTypes.string,
      language_code: PropTypes.string,
      _id: PropTypes.string.isRequired,
    })
  ),
  initWaitersAction: PropTypes.func.isRequired,
  updateAdmin: PropTypes.func.isRequired,
  updateWaiter: PropTypes.func.isRequired,
  deleteWaiterByTgId: PropTypes.func.isRequired,
  deleteAdminByTgId: PropTypes.func.isRequired,
}

export default React.memo(Waiters);
