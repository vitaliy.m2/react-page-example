import _get from 'lodash/get';
import {
  WAITERS_SET_WAITERS_LIST,
  WAITERS_SET_LOADING,
  WAITERS_UPDATE_WAITER_INFO,
  WAITERS_DELETE_WAITER_BY_TG_ID,
} from 'actionTypes/waiters';
import { get, put, destroy } from 'utils/api';
import { getAllQrs } from 'actions/qr';
import { getAdmins } from 'actions/admins';

export const setWaitersList = (data) => (dispatch) => {
  dispatch({
    type: WAITERS_SET_WAITERS_LIST,
    payload: data,
  });
};

export const setLoading = (status) => (dispatch) => {
  dispatch({
    type: WAITERS_SET_LOADING,
    payload: status,
  });
};

export const updateWaiterInfo = (waiterInfo) => (dispatch) => {
  dispatch({
    type: WAITERS_UPDATE_WAITER_INFO,
    payload: waiterInfo,
  });
};

export const deleteWaiterByTgIdAction = (waiterTgId) => (dispatch) => {
  dispatch({
    type: WAITERS_DELETE_WAITER_BY_TG_ID,
    payload: waiterTgId,
  });
};

export const getWaiters = () => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await get('waiters');
    const waiters = _get(response, 'waiters', []);
    dispatch(setWaitersList(waiters));
    dispatch(setLoading(false));
  } catch (e) {
    console.error(e);
  }
};

export const initWaitersAction = () => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    await dispatch(getWaiters());
    await dispatch(getAdmins());
    await dispatch(getAllQrs());
    dispatch(setLoading(false));
  } catch (e) {
    console.error(e);
  }

}

export const saveWaiter = (data) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await put('waiters', data);
    const waiters = _get(response, 'waiters', [])
    dispatch(setWaitersList(waiters))
    dispatch(setLoading(false));
  } catch (e) {
    console.error(e);
  }
};

export const updateWaiter = (waiterInfo) => async (dispatch) => {
  try {
    dispatch(setLoading(true));
    const response = await put('waiter', waiterInfo);
    if (response) {
      dispatch(updateWaiterInfo(waiterInfo));
    }
    dispatch(setLoading(false));
    return response;
  } catch (e) {
    dispatch(setLoading(false));
    console.error(e);
    return _get(e, 'response.data', null);
  }
};

export const deleteWaiterByTgId = (waiterTelegramId) => async (dispatch) => {
  try {
    const response = await destroy('waiters', {
      waiterTelegramId,
    });
    if (response) {
      dispatch(deleteWaiterByTgIdAction(waiterTelegramId))
    }
  } catch (e) {
    console.error(e);
  }
};
