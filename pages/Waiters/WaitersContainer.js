import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { initWaitersAction, updateWaiter, deleteWaiterByTgId } from 'actions/waiters';
import { updateAdmin, deleteAdminByTgId } from 'actions/admins';
import { waitersWithQrSelector } from 'selectors';
import Waiters from 'pages/Waiters/Waiters';

const mapStateToProps = ({
  waiters: { waitersList, waitersLoading },
  admins: { adminsList, adminsLoading },
  qr: { qrList }
}) => ({
  waitersLoading,
  adminsLoading,
  waitersList: waitersWithQrSelector(waitersList, qrList),
  adminsList,
});

const mapDispatchToProps = {
  initWaitersAction,
  updateAdmin,
  updateWaiter,
  deleteWaiterByTgId,
  deleteAdminByTgId,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(withRouter(Waiters))
